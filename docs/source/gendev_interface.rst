===========================
The GenericDevice Interface
===========================

The library aims to offer an unified API for accessing multiple devices related
to the µTCA systems.

Two classes are defined within the :py:mod:`gendev_tools.gendev_interface`
module:

1. The class :py:class:`gendev_tools.gendev_interface.ConnType`.
2. The class :py:class:`gendev_tools.gendev_interface.GenDevInterface`.

ConnType
========

This class defines a list of the allowed communication interfaces for any
device supported by a module implementing the **GenDevInterface**.

Some µTCA devices offer different management interfaces such as: **Telnet**,
**SSH**, or a serial communication port. Meaning that any module aiming to
offer a driver for a particular device, might support several communication
methods to the physical device. Or that a driver should check whether a request
is supported using the provided communication method or not. This scenario is
well seen in the driver for the NAT MCH.

GenDevInterface
===============

This class defines the API that should be implemented by any driver within
this library. The main features covered by the API are:

- Retrieve the information and status of a device.
- Access/Modify configuration parameters of a device.
- Check the configuration parameters of a device.
- Update the fw of a device.
- Enable DHCP for a device supporting Ethernet connections.

The previous list includes a very generic set of operations. µTCA devices are
very heterogeneous, and not all of them offer the same management interfaces.
The goal for this API was defining a minium set of operations that could be
supported by any driver for a µTCA device regardless of existing differences.
Then, if the API is generic, but the real hardware is not, how a driver can
address the existing differences? The way to customize an implementation for
the needs of a particular device is by means of the given data to the
drivers.

Most of the methods defined in the API expect/deliver a dictionary, having one
or many hierarchy levels. This way, any driver can define the strcture and
content for such data in order to fit the needs of a device. In brief, the
IO of the methods is clearly defined but its content is not. Inspect the
documentation for the :py:class:`gendev_tools.gendev_interface.GenDevInterface`
module or directly dive into some of the driver's documentation.
