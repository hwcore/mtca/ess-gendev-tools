==============================
ESS Generic Device Tools Pylib
==============================

.. image:: https://gitlab.esss.lu.se//hwcore/mtca/ess-gendev-tools/badges/master/pipeline.svg
.. image:: https://sonarqube.esss.lu.se/api/project_badges/measure?project=gendev-tools&metric=alert_status
.. image:: https://sonarqube.esss.lu.se/api/project_badges/measure?project=gendev-tools&metric=ncloc
.. image:: https://sonarqube.esss.lu.se/api/project_badges/measure?project=gendev-tools&metric=coverage
.. image:: https://readthedocs.org/projects/ess-generic-devices-tools-pylib/badge/?version=latest

This Python library is scoped within the µTCA management tools project. The
purpose of this library is providing a set of modules for controlling some
of the most common devices that our group installs in the µTCA systems.

Using this library, most of the annoying details about handling µTCA based
systems are hidden. The main aim of this library is delivering an easy and
reliable interface to some of the devices that are commonly found in a µTCA
system.

.. _`Readthedocs`: https://ess-generic-devices-tools-pylib.readthedocs.io/en/latest/

A compiled version of the package documentation is available at `Readthedocs`_.

Supported devices
=================

Currently, the following devices are supported by the library:

- **NAT MCH**

If you'd like to add support for another device, contact any of the maintainers
or feel free to contribute! But, please, take a look at the
:doc:`../contributing` guidelines first.

Library installation
====================

Stable verions of the library can be obtained from Pypi:

    $ pip install gendev-tools

For development versions, download the repository instead. When aiming to
install the library system-wide, avoid the steps concerning *virtualenv*.

.. code-block:: bash

    $ git clone https://gitlab.esss.lu.se/hwcore/mtca/ess-gendev-tools.git <target branch>
    $ cd ess-gendev-tools
    $ virtualenv env
    $ source env/bin/activate
    $ python -m pip install --upgrade pip
    $ pip install build
    $ python -m build
    $ pip install -e .

You're ready to go!

Using the library
=================

This repository is not intended to include any user application. It rather
provides drivers and other modules to interface devices. Some code snippets
have been include to illustrate the usage of the library, though. Check the
following sections for code examples of the library's modules:

- MCH module : :doc:`./natmch_usage`
